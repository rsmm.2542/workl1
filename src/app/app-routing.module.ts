import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowcardModule } from './showcard/showcard.module';

const routes: Routes = [
  {path:'setting',
  loadChildren:()=>
    import('./table/table.routing.module').then((show)=> show.TableRoutingModule)
  },
  {
    path: 'showcard',
    loadChildren:()=>
    import('./showcard/showcard.routing.module').then((show)=> show.ShowcardRoutingModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
