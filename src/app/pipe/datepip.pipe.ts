import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'datepip'
})
export class DatepipPipe implements PipeTransform {

  //timeuser:Timeuser[]=[]
  time:number|undefined
  transform(timeday:string|any) {

    let dd = new Date().getTime()
    const date = new Date(timeday).getTime();
    var ageDifMs = dd - date;
    var ageDate = new Date(ageDifMs); 
    var re = Math.abs(ageDate.getUTCFullYear() - 1970)

    var d =  Math.abs(ageDate.getDay() + 7)
    var set = re+' ปี '+ageDate.getMonth()+' เดือน '+d+' วัน '
    if (re==0){
      var set = ageDate.getMonth()+' เดือน '+d+' วัน '
    }
    if (ageDate.getMonth()==0){
      var set =d+' วัน '
    }
  
    
    return set;
}
}
