import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatepipPipe } from './datepip.pipe'



@NgModule({

  // declarations: [CalculatePipe],
  imports: [CommonModule],

  declarations: [
     DatepipPipe

  ],
  exports:[DatepipPipe]
  // exports: [CalculatePipe],
 
})
export class ModulePipeModule { }
