import { Injectable, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  FormGroup,FormControl,Validators, FormBuilder } from '@angular/forms';
import { Datepeople } from '../model/table';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ServicePeopleService {
  people: any;
  editpeople:Datepeople[]=[];
  allpeople1:Datepeople[] =[];
  dataSource = new MatTableDataSource<Datepeople>(this.allpeople1);
  displayedColumns: string[] = ['demo-position', 'demo-name', 'demo-sex', 'demo-date','demo-type','demo-setting'];

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  constructor(private http:HttpClient) { 
    

  }

  adddata1(add:Datepeople):Observable<Datepeople[]>{
  
    return this.http.post<Datepeople[]>('http://127.0.0.1:80/database/add',JSON.stringify({
      name: add.name,
      sex:  add.sex,
      date: add.date,
      type: add.type
    }))
  }

  editdata1(data:number,edit:Datepeople):Observable<Datepeople[]>{
    console.log('data: '+data)
    return this.http.post<Datepeople[]>('http://127.0.0.1:80/database/update',JSON.stringify({
      ID:data,
      name:edit.name,
      type:edit.type
    }))
  }
  delect(ID: number){
    console.log(ID);
    this.http.get<number>(' http://127.0.0.1:80/database/delect/'+ID).subscribe(data => {
      alert('ลบข้อมูลสำเร็จ');
      location.reload();
  })
  }
  showdata(path : string):Observable<Datepeople[]>{
    return this.http.get<Datepeople[]>(path).pipe(
      map((allpeople:Datepeople[])=>{
        return allpeople.map(allpeople => ({
          ID:allpeople.ID,
          name:allpeople.name,
          sex:allpeople.sex,
          date:allpeople.date,
          type:allpeople.type
        }))
      })
    );
  }
  types: Type1[] = [
    {value: 'Pfizer', viewValue: 'Pfizer'},
    {value: 'Moderna', viewValue: 'Moderna'},
    {value: 'Johnson & Johnson', viewValue: 'Johnson & Johnson'},
    {value: 'Novavax', viewValue: 'Novavax'},
    {value: 'AstraZeneca', viewValue: 'AstraZeneca'},
    {value: 'Sputnik-V', viewValue: 'Sputnik-V'},
    {value: 'Sinovac', viewValue: 'Sinovac'},
    {value: 'Sinopharm', viewValue: 'Sinopharm'},
    {value: 'CanSino', viewValue: 'CanSino'},
    {value: 'Covishield', viewValue: 'Covishield'},
    {value: 'Covaxin', viewValue: 'Covaxin'},
  ];


}
interface Type1{
  value: string;
  viewValue: string;
}
