import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Datepeople } from '../../model/table';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import {Location} from '@angular/common';
@Component({
  selector: 'app-showdata',
  templateUrl: './showdata.component.html',
  styleUrls: ['./showdata.component.css']
})
export class ShowdataComponent implements OnInit {
  IDUser:number|undefined
  allpeople1:Datepeople[] =[];
  people:Datepeople[]=[];
  constructor(public route:ActivatedRoute, private _location:Location) { 

    let ID = this.route.snapshot.params['ID'];
    this.IDUser= ID;
  }

  ngOnInit(): void {
    this.showUser()
  }
  
  showUser(){
    const peopleJson = localStorage.getItem('allpeople');
    this.allpeople1 = peopleJson !== null ? JSON.parse(peopleJson) : null;
    for(let user of this.allpeople1){
      switch(this.IDUser){
        case user.ID:{
          this.people.push(user)
        }
      }
     
    }

}
backClicked() {
  this._location.back();
}






}
