import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Datepeople } from '../../model/table';
import { ServicePeopleService } from '../../service/service-people.service';

// import {describe} from '../pipe/calculate.pipe'
@Component({
  selector: 'app-showcard',
  templateUrl: './showcard.component.html',
  styleUrls: ['./showcard.component.css']
})
export class ShowcardComponent implements OnInit {
  allpeople1:Datepeople[] =[];
  people:Datepeople[] =[];
  name1:string|undefined
  ID!:number
  constructor(public Service:ServicePeopleService,public http:HttpClient) { 
 
  }

  ngOnInit(): void {
   this.showdata();
   
  }
  showdata(){
        const peopleJson = localStorage.getItem('allpeople');
        this.allpeople1 = peopleJson !== null ? JSON.parse(peopleJson) : null;
        for(let user of this.allpeople1){
            this.ID=user.ID
        }
  }
 
}
