import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowdataComponent } from './showdata/showdata.component';
import {ShowcardComponent}from'./showcard/showcard.component'

const routes: Routes = [
      {path:'',
        component:ShowcardComponent
      },
      {path:':ID',
        component:ShowdataComponent
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowcardRoutingModule { }
