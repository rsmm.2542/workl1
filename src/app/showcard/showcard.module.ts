import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowdataComponent } from './showdata/showdata.component';
import {ShowcardComponent}from'./showcard/showcard.component'
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import {RouterModule} from '@angular/router';
import { ModulePipeModule } from '../pipe/module-pipe.module';


@NgModule({
  declarations: [ShowcardComponent,
    ShowdataComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatGridListModule,
    ModulePipeModule,
    RouterModule
  ],
  exports:[ShowcardComponent,
    ShowdataComponent]
})
export class ShowcardModule { }
