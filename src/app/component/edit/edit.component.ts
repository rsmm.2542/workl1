import { HttpClient } from '@angular/common/http';
import { Component, OnInit,Input, Inject } from '@angular/core';
import {ServicePeopleService} from '../../service/service-people.service';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Datepeople } from '../../model/table';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})

export class EditComponent implements OnInit {

  allpeople1:Datepeople[]=[]
  User:Datepeople[]=[]
  actionEdit:string='edit'
  data1:number=this.data
  constructor( public http: HttpClient,public Service:ServicePeopleService,public dialog:MatDialog,
    @Inject(MAT_DIALOG_DATA) public data:number) { }

  ngOnInit() {
    this.getUser()
  }

  getUser(){
    const peopleJson = localStorage.getItem('allpeople');
    this.allpeople1 = peopleJson !== null ? JSON.parse(peopleJson) : null;
    for(let user of this.allpeople1){
      switch(this.data){
        case user.ID:{
          this.User.push(user)
        
        }
      }
     
    }

  }

}











