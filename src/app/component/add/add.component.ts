import { Component, Input, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap'
import {  FormGroup,FormControl,Validators, FormBuilder } from '@angular/forms';
import {ServicePeopleService} from '../../service/service-people.service';
import {MatDialog,MatDialogConfig} from '@angular/material/dialog';
import { Datepeople } from '../../model/table';
//import { } from'../dialog/dialog.component';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {
  peopleadd:Datepeople[]=[];
  actionadd:string="add"
  constructor(private modalService: NgbModal, public formBuilder: FormBuilder,public Service:ServicePeopleService,public dialog: MatDialog) { 
   
  }
  ngOnInit(): void {
    
  }

}


