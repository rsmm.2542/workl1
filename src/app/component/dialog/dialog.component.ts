import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ServicePeopleService } from '../../service/service-people.service';
import { dataForm, Datepeople } from '../../model/table';
import {Location} from '@angular/common';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  @Input() name?: string;
  @Input() type?: string;
  @Input() action?: string;
  

  UserID!: number;
  get ID(): number {
    return this.UserID;
}
@Input() set ID(id:number){
    this.UserID = id
  };
  
  

  name_Edit?:String
  type_Edit?:string
  people = this.formBuilder.group({
    name: ['',Validators.required],
    sex: [''],
    date: [''],
    type: ['',Validators.required]
  });
  constructor(public dialog: MatDialogRef<DialogComponent>,public Service:ServicePeopleService ,
    private formBuilder: FormBuilder,public _location:Location) {
     
    

   }

  ngOnInit(): void {
    this.editnametype()
    console.log(this.UserID)
   

  }
  editnametype(){
    if(this.name==null&&this.type==null){
      this.name_Edit = 'ชื่อ-สกุล'
      this.type_Edit = 'ชนิดวัคซีน'
    }
    else{
      this.name_Edit =this.name
      this.type_Edit =this.type
    }
  }
  onSubmit(): void{
    if(this.action=='add'){
          this.Service.adddata1(this.people.value).subscribe((people:Datepeople[])=>{
          location.replace('setting');
    })
    }else if(this.action == 'edit'){
      this.Service.editdata1(this.UserID, this.people.value).subscribe((people:Datepeople[])=>{
        this.dialog.close()
        location.replace('setting');
      },(error)=>{});

    }else{

    }


  };
  cancel(){
    this.dialog.close()
  }
  setValue(){
    //this.people.PatchValue()
  }
}
