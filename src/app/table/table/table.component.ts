import { Component, OnInit,Input, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  EditComponent } from '../../component/edit/edit.component';
import { Datepeople } from '../../model/table';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ServicePeopleService} from '../../service/service-people.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddComponent } from '../../component/add/add.component';



@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  entryComponents : [EditComponent]
})
export class TableComponent implements OnInit {
  displayedColumns: string[] = ['demo-position', 'demo-name', 'demo-sex', 'demo-date','demo-type','demo-setting'];
  allpeople1:Datepeople[] =[];
  dataSource = new MatTableDataSource<Datepeople>(this.allpeople1);
  path:string ="http://127.0.0.1:80/database/select";

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor( private http: HttpClient,public Service:ServicePeopleService,public dialog: MatDialog) { }
  

  async ngOnInit(){

    this.Service.showdata(this.path).subscribe((people:Datepeople[])=>{
      this.allpeople1 = people
      localStorage.setItem('allpeople',JSON.stringify(this.allpeople1));
      this.dataSource = new MatTableDataSource<Datepeople>(this.allpeople1);
      this.dataSource.paginator = this.paginator;
    })

  }

  search(term : string) {
    if(term != ''){
      this.path='http://127.0.0.1:80/database/search/'+term
      this.Service.showdata(this.path).subscribe((people:Datepeople[])=>{
        this.allpeople1 = people
        this.dataSource = new MatTableDataSource<Datepeople>(this.allpeople1);
        this.dataSource.paginator = this.paginator;
      })

    }else{
      this.Service.showdata(this.path).subscribe((people:Datepeople[])=>{
        this.allpeople1 = people
        this.dataSource = new MatTableDataSource<Datepeople>(this.allpeople1);
        this.dataSource.paginator = this.paginator;
      })
    }
    
    

  }
  openDialogadd() {
    const dialogConfig = new MatDialogConfig();
    const dialogRef = this.dialog.open(AddComponent,dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      dialogRef.close();
    });


  }
  openDialogedit(data:number) {
    const dialogRef = this.dialog.open(EditComponent,{data});
    dialogRef.afterClosed().subscribe(() => {
      dialogRef.close();
    })

  }
}
