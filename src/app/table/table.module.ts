import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule} from '@angular/material/form-field';
import { TableComponent } from './table/table.component';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatIconModule} from '@angular/material/icon';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatTableModule} from '@angular/material/table';
import { MatSelectModule} from '@angular/material/select';
import { MatRadioModule} from '@angular/material/radio';
import { MatNativeDateModule} from '@angular/material/core';
import { MatDialogModule} from '@angular/material/dialog';
import { MatInputModule} from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
@NgModule({
  declarations: [TableComponent],
  
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatIconModule,
    MatDatepickerModule,
    MatTableModule,
    MatSelectModule,
    MatRadioModule,
    MatNativeDateModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    MatGridListModule
  ],
  exports:[TableComponent]
})
export class TableModule { }
